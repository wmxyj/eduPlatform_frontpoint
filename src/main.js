import Vue from 'vue'
import VueRouter from 'vue-router'
import Vuex from 'vuex'
import globalConstant from "./util/global";
import axios from 'axios'
import i18n from './i18n/i18n'

Vue.use(i18n);

axios.defaults.withCredentials = true;

import echarts from 'echarts'
import URL from './util/url'
import 'echarts/dist/echarts.min.js'
import 'font-awesome/css/font-awesome.min.css'

import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
import Vuetify from 'vuetify'
import 'vuetify/dist/vuetify.min.css'
import Vuelidate from 'vuelidate'
import vueGridLayout from 'vue-grid-layout';

import App from './App'
import store from './store'
import util from './util/util'
import resource from './resource'

import {routers} from './router'

import Bus from './util/bus'
import './util/directives'
import {simpleGet, get, patch, post, put, remove, postAdvanced} from './util/axios'
import {time2FormatStr, formatStr2Time} from './util/date'
import mapBoxGl from 'mapbox-gl'


Vue.use(VueRouter);
Vue.use(Vuex);
Vue.use(ElementUI);
Vue.use(Vuetify);
Vue.use(Vuelidate);
Vue.use(echarts);
Vue.use(vueGridLayout);
Vue.prototype.$globalConstant = globalConstant;
Vue.prototype.Mapbox = mapBoxGl;
Vue.prototype.$Bus = Bus;
Vue.prototype.$URL = URL;
//定义axios标签
Vue.prototype.$axios = {
    get,
    post,
    patch,
    put,
    remove,
};
Vue.prototype.$date = {
    time2FormatStr,
    formatStr2Time,
};

Vue.config.productionTip = false;

// 路由配置
const RouterConfig = {
    mode: 'hash',
    routes: routers
};
const router = new VueRouter(RouterConfig);

router.beforeEach((to, from, next) => {
    let currentPageTitle = to.meta.title;
    if (to.matched.some(m => m.meta.requireAuth)) {
    } else {
        next();
    }
    if (to.fullPath == '/platform') {
        globalConstant.page = 'platform'
    } else {
       globalConstant.page = '';
    }
    util.title(currentPageTitle)//设置网页标题
});

router.afterEach((to, from, next) => {
    window.scrollTo(0, 0)
});

/* eslint-disable no-new */
new Vue({
    el: '#app',
    store,
    i18n,
    router,
    resource,
    template: '<App/>',
    components: {App}
});