import manager from '../views/manager.vue'
import Main from '../views/index.vue'
import Navigation from '../views/navigation.vue'
import Empty from '../views/empty.vue'
import map_box from '../views/mapbox'
export const appRouter = [
    {
        meta: {
            title:'首页',
            title2:'home'
        },
        path: '/index',
        name: 'index',
        component: Navigation,
        iconCls: 'el-icon-message',
    },
    {
        meta: {
            title: '学术前沿',
            title2:'academia'
        },
        path: '/academia',
        component: Empty,
        name: 'academia',
        iconCls: 'el-icon-message',
    },
    {
        meta: {
            title: '数据目录',
            title2:'dataCatalog'
        },
        path: '/dataCatalog',
        component: Empty,
        name: 'dataCatalog',
        iconCls: 'el-icon-message',
    },
    {
        meta: {
            title: '在线工具',
            title2:'webTool'
        },
        path: '/webTool',
        component: Empty,
        name: 'webTool',
        iconCls: 'el-icon-message',
    },
    {
        meta: {
            title: '网络资源',
            title2:'netResource'
        },
        path: '/netResource',
        component: Empty,
        name: 'netResource',
        iconCls: 'el-icon-message',
    },
    {
        meta: {
            title: '团队介绍',
            title2:'teamInfo'
        },
        path: '/teamInfo',
        component: Empty,
        name: 'teamInfo',
        iconCls: 'el-icon-message',
    },
    {
        meta: {
            title: '教学案例',
            title2:'eduCase'
        },
        path: '/eduCase',
        component: Empty,
        name: 'eduCase',
        iconCls: 'el-icon-message',
    },
    {
        meta: {
            title: '可视化平台',
            title2:'platform'
        },
        path: '/platform',
        component: map_box,
        name: 'platform',
        iconCls: 'el-icon-message',
    }
];

export const mainRouter = {
    path: '/',
    redirect: '/index',
    name: 'index',
    meta: {
        title: '首页',
        title2:'HOME'
    },
    component: Main,
    children: appRouter
};
export const managerRouter = {
    path: '/manager',
    name: 'manager',
    meta: {
        title: '管理系统',
        title2:'HOME'
    },
    icon: 'fa fa-user-circle-o fa-lg',
    component: manager,
};


export const routers = [
    mainRouter,
    managerRouter,
];
